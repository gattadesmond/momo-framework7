import $$ from 'dom7';
import { Device, Request, Utils } from 'framework7';
import Framework7, {Template7} from './framework7-custom.js';
import {Date1,Number1,EncodeURI} from './MoMoPwa.Helpers.Format.js';
import Framework7Keypad from 'framework7-plugin-keypad';
// install plugin
Framework7.use(Framework7Keypad);
// import framework7 modules/components we need on initial page
//import Toolbar from 'framework7/components/toolbar/toolbar.js';

// import Swiper from 'framework7/components/swiper/swiper.js';
//Sai co ban la nhiu day
// install core modules
//Framework7.use([Toolbar]);

// Import F7 Styles
import '../css/framework7-custom.less';

// Import Icons and App Custom Styles
import '../css/icons.css';
import '../css/app.less';

import '../css/core.css';
// Import Routes
import routes from './routes.js';

// Import main app component
//import App from '../app.f7.html';



window.MoMoPwa = new Framework7({
  root: '#app', // App root element
  //component: App, // App main component

  name: 'Momo In-App', // App name
  theme: 'ios', // Automatic theme detection
  // App root data
  view: {
    xhrCache: false,
    pushState: true,
    pushStateSeparator: "",
    allowDuplicateUrls: false,
    preloadPreviousPage: false,
    stackPages: true,
    iosDynamicNavbar: false,
  },
  sheet: {
    backdrop: true
  },
  dialog: {
    title: 'Thông báo',
    buttonOk: 'Đồng ý'
  },
  webView: true,
  // App routes
  routes: routes,
  // Register service worker
  on: {
    init: function () {
        Template7.registerHelper('FM_Date1', function (date, format, options) {
            if (!date)
                return date;
            date = new Date(date);
            return Date1(date, format);
        });
        Template7.registerHelper('FM_Number1', function (value, options) {
            return Number1(value);
        });
        Template7.registerHelper('FM_EncodeURI', function (value, options) {
            return EncodeURI(value); 
        });
    }
}
});

Framework7.request.setup({
  headers: {
    'X-Requested-With': 'XMLHttpRequest'
  }
})