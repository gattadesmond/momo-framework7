import Heodihoc from '../pages/heodihoc/index.f7.html';
import Article from '../pages/article/detail-template.f7.html';
import ArticlePreview from '../pages/article/preview.f7.html';
import ArticleGuide from '../pages/guide/index.f7.html';
import ArticleGuideDetail from '../pages/guide/detail.f7.html';
import GuideDetail from '../pages/singleguide/detail.f7.html';
import DonationHome from '../pages/donation/index.f7.html';
import DonationDetail from '../pages/donation/detail.f7.html';
import DonationTrans from '../pages/donation/confirm.f7.html';
import DonationTransSuccess from '../pages/donation/confirm-success.f7.html';
import DonationTransFail from '../pages/donation/confirm-fail.f7.html';
import DonationHeoHome from '../pages/donationheovang/index.f7.html';
import DonationHeoDetail from '../pages/donationheovang/detail.f7.html';

import RandomVoucher from '../pages/randomvoucher/detail.f7.html';
import Sucmanh2k from '../pages/sucmanh2k/index.f7.html';
import GioiThieuBanBe from '../pages/gtbb/index.f7.html';
import HDLKAgribank from '../pages/hdlk-agribank/index.f7.html';

import Cashback from '../pages/cashback/index.f7.html';
import CashbackFilter from '../pages/cashback/index.f7.html';
import CashbackFilterAll from '../pages/cashback/indexall.f7.html';
import CashbackAll from '../pages/cashback/indexall.f7.html';
import CashbackDetail from '../pages/cashback/detail.f7.html';
import CashbackDetailFilter from '../pages/cashback/detail.f7.html';

import Dev from '../pages/devview/index.f7.html';
import NotFound from '../pages/404.f7.html';
import Loading from '../pages/loading.f7.html';

var routes = [
    {
        name: "cashback-filter",
        path: "/hoan-tien/c:cateId-ct:citId-dt:dtId-g:group",
        options: {
            history: true
        },
        component: CashbackFilter
    },
    {
        name: "cashback",
        path: "/hoan-tien",
        options: {
            history: true
        },
        component: Cashback
    },
    {
        name: "cashback-filter-all",
        path: "/hoan-tien-all/c:cateId-s:sortType-ct:citId-dt:dtId",
        options: {
            history: true
        },
        component: CashbackFilterAll
    },
    {
        name: "cashback-all",
        path: "/hoan-tien-all",
        options: {
            history: true
        },
        component: CashbackAll
    },
    {
        name: "cashback-detail",
        path: "/hoan-tien/:brandSlug-:brandId",
        options: {
            history: true
        },
        component: CashbackDetail
    },
    {
        name: "cashback-detail-filter",
        path: "/hoan-tien/:brandSlug-:brandId/ct:citId-dt:dtId",
        options: {
            history: true
        },
        component: CashbackDetailFilter
    },
    {
        name: 'donate-home',
        path: "/quyen-gop",
        options: {
            history: true
        },
        component: DonationHome
    },
    {
        name: 'donate-detail',
        path: "/quyen-gop/:slug",
        options: {
            history: true
        },
        component: DonationDetail
    },
    {
        name: 'donate-confirm-fail',
        path: "/dong-gop-that-bai/:token",
        options: {
            history: true
        },
        component: DonationTransFail
    },
    {
        name: 'donate-confirm-success',
        path: "/dong-gop-thanh-cong/:token",
        options: {
            history: true
        },
        component: DonationTransSuccess
    },
    {
        name: 'donate-trans',
        path: "/xac-nhan-dong-gop/:token",
        options: {
            history: true
        },
        component: DonationTrans
    },
    {
        name: 'donate-home-heovang',
        path: "/quyen-gop-heovang",
        options: {
            history: true
        },
        component: DonationHeoHome
    },
    {
        name: 'donate-detail-heovang',
        path: "/quyen-gop-heovang/:slug",
        options: {
            history: true
        },
        component: DonationHeoDetail
    },
    {
        name: 'heodihoc',
        path: "/heodihoc",
        options: {
            history: true
        },
        component: Heodihoc
    },
    {
        name: "article-detail",
        path: "/news/:category/:slug-:id(\\d+)",
        options: {
            history: true
        },
        component: Article
    },
    {
        name: "article-preview",
        path: "/news-preview/:category/:slug-:id",
        options: {
            history: true
        },
        component: ArticlePreview
    },
    {
        name: "bankmapping",
        path: "/lien-ket-tai-khoan",
        options: {
            history: true
        },
        component: ArticleGuide
    },
    {
        name: "bankmapping-detail",
        path: "/lien-ket-tai-khoan/:slug",
        options: {
            history: true
        },
        component: ArticleGuideDetail
    },
    {
        name: "guide-detail-single",
        path: "/guide/huong-dan-:id",
        options: {
            history: true
        },
        component: GuideDetail
    },
    {
        name: "random-voucher",
        path: "/random-voucher",
        options: {
            history: true
        },
        component: RandomVoucher
    },
    {
        name: "donation-2000",
        path: "/sucmanh2k",
        options: {
            history: true
        },
        component: Sucmanh2k
    },
    {
        name: "hdlk-agribank",
        path: "/hdlk-agribank",
        options: {
            history: true
        },
        component: HDLKAgribank
    },
    {
        name: "gtbb",
        path: "/gtbb",
        options: {
            history: true
        },
        component: GioiThieuBanBe
    },
    {
        name: "dev-view",
        path: "/dev",
        options: {
            history: true
        },
        component: Dev
    },
    {
        name: '404',
        path: "/404",
        component: NotFound
    },
    {
        name: 'Loading',
        path: "/loading",
        component: Loading
    },
    {
        path: "(.*)",
        component: NotFound
    }
];

export default routes;