const env = process.env.NODE_ENV || 'development';

const System = env === 'production' ? {apihost: "http://dev1.momo.vn:6969",userInfo: {}, HostUrl : location.origin} : {apihost: "http://localhost:51166",userInfo: {phone:"0772532512",userName:"MarTech Test",balance:300000,appVer:21422,appCode:"2.1.42",deviceOS:"IOS"}, HostUrl : location.origin};
//{apihost: "http://dev1.momo.vn:6969",userInfo: {phone:"0772532512",userName:"MarTech Test",balance:300000,appVer:21422,appCode:"2.1.42",deviceOS:"IOS"}
const ApiResponseErrorCodeMessage = {
    request_error: "Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!",
    system_error: "Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!",
    token_error: "Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!",
    notfound: '404! Không tìm thấy dữ liệu',
    donate_error_1014: 'Giao dịch thất bại vì mật khẩu không chính xác. Vui lòng thử lại.',
    donate_error_1000: 'Vui lòng nhập mật khẩu 6 mã PIN',
    donation_createtrans_errortotal: 'Vui lòng nhập số tiền muốn Quyên góp',
    session_error: 'Về màn hình chính',
    session_error_content: 'Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!',
    cashback_game_missing: 'Chương trình chưa bắt đầu. Bạn vui lòng thử lại lần sau!',
    cashback_game_submit_error: 'Lỗi xử lý kết quả câu hỏi',
    heodihoc_error: 'Về màn hình nhiệm vụ',
    heodihoc_session_error: 'Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!',
    heodihoc_noquiz: 'Lớp học chưa mở. Vui lòng thử lại sau ít phút!',
    heodihoc_submiterror: 'Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!'
};

const Track = (app, dataTrack) => {
    var url = System.apihost + '/__post/Ajax/TrackInfo';

    var param = {
        data: dataTrack
    };

    app.request.post(url, param, (res) => {
    }, (err) => { }, 'json');
}

const UpdateCurrentLocation = (location) => {
    alert("Lấy Location Thành Công " + location);
}

const ProcessShareFacebook = (url, isIos) => {
    var now = new Date().valueOf();
    setTimeout(function () {
        if (new Date().valueOf() - now > 500) return;
        window.location = "momo://?refId=browser|https://facebook.com/sharer/sharer.php%3Fu%3D" + encodeURIComponent(url);
    }, 200);

    if (isIos == undefined) {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            isIos = true;
        }
        else {
            isIos = false;
        }
    }

    if (isIos) {
        window.location = "fbapi://dialog/share?app_id=966242223397117&amp;version=20130410&amp;method_args=%7B%22name%22%3Anull%2C%22description%22%3Anull%2C%22link%22%3A%22" + encodeURIComponent(url) + "%22%2C%22quote%22%3Anull%2C%22hashtag%22%3Anull%2C%22dataFailuresFatal%22%3A%22false%22%7D";
    }
    else {
        window.location = "fb://faceweb/f?href=https://facebook.com/sharer/sharer.php?u=" + encodeURIComponent(url);
    }
}

const ProcessAppTracking = (event, action) => {
    var objTrack = { action: 'trackingEvent', value: { event: event, param: { action: action } } };
    window.ReactNativeWebView.postMessage(JSON.stringify(objTrack));
}

const ProcessAppTrackingCustomParam = (event, param) => {
    var objTrack = { action: 'trackingEvent', value: { event: event, param: param } };
    window.ReactNativeWebView.postMessage(JSON.stringify(objTrack));
}

const ProcessAppShareFacebook = (url) => {
    var objShare = { action: "shareFacebookUrl", value: url };
    window.ReactNativeWebView.postMessage(JSON.stringify(objShare));
}

const ProcessAppShareImageFacebook = () => {
    var objShare = { action: "shareScreenShot" };
    window.ReactNativeWebView.postMessage(JSON.stringify(objShare));
}

const ProcessAppShareOther = (url) => {
    var objShare = { action: "shareContent", value: url };
    window.ReactNativeWebView.postMessage(JSON.stringify(objShare));
}

const CallbackAppUserInfo = (userData) => {
    MoMoPwa.emit('initPageData', userData);
}

const CallbackPasswordInfo = (userData) => {
    MoMoPwa.emit('CallbackPasswordInfo', userData);
}

const CallbackAppFwFunc = (input) => {
    MoMoPwa.emit(func, data);
}

window.CallbackAppUserInfo = function(userData) {
    MoMoPwa.emit('initPageData', userData);
}

window.CallbackPasswordInfo = function(userData) {
    MoMoPwa.emit('CallbackPasswordInfo', userData);
}

window.CallbackAppFwFunc = function(func, data) {
    MoMoPwa.emit(func, data);
}

export {
    System, ApiResponseErrorCodeMessage, ProcessShareFacebook, ProcessAppTracking,
    ProcessAppTrackingCustomParam, ProcessAppShareFacebook, ProcessAppShareImageFacebook, ProcessAppShareOther, CallbackAppUserInfo, 
    CallbackPasswordInfo, CallbackAppFwFunc,Track
}